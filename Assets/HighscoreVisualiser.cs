using Assets.Scripts;
using TMPro;
using UnityEngine;

namespace Assets
{
    public class HighscoreVisualiser : MonoBehaviour
    {
        [SerializeField] private TMP_Text highscoreText;

        private void Awake()
        {
            MessageDistributor.AddObserver(MessageType.HighscoreChanged, UpdateNewHighscore);
        }

        private void Start()
        {
            UpdateHighscore();
        }

        private void UpdateHighscore()
        {
            var highscore = RolloballManager.Instance.GetHighscore();
            highscoreText.text = $"${highscore}";
        }

        private void UpdateNewHighscore()
        {
            var highscore = RolloballManager.Instance.GetHighscore();
            highscoreText.text = $"<color=#786F11FF>NEW!</color> ${highscore}";
            Invoke(nameof(UpdateHighscore), 3f);
        }
    }
}
