using Assets.Scripts.Physics;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(MyCubeCollider))]
    public class MyCubeColliderEditor : UnityEditor.Editor
    {
        private MyCubeCollider _cubeCollider;

        public void OnSceneGUI()
        {
            _cubeCollider = target as MyCubeCollider;
            Handles.color = Color.green;
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
            var cubeColliderTransform = _cubeCollider.transform;
            var position = cubeColliderTransform.position;
            Handles.DrawWireCube(position, _cubeCollider.DimensionsWithScale);
        }
    }
}