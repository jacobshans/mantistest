using Assets.Scripts.Physics;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(MySphereCollider))]
    public class MySphereColliderEditor : UnityEditor.Editor
    {
        private MySphereCollider _sphereCollider;

        public void OnSceneGUI()
        {
            _sphereCollider = target as MySphereCollider;
            Handles.color = Color.green;
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
            var sphereColliderTransform = _sphereCollider.transform;
            var position = sphereColliderTransform.position;
            Handles.DrawWireDisc(position, sphereColliderTransform.forward, _sphereCollider.RadiusWithScale);
            Handles.DrawWireDisc(position, sphereColliderTransform.up, _sphereCollider.RadiusWithScale);
            Handles.DrawWireDisc(position, sphereColliderTransform.right, _sphereCollider.RadiusWithScale);
        }
    }
}