using Assets.Scripts;
using Assets.Scripts.Inventory;
using UnityEngine;

namespace Assets
{
    public class RolloballManager : SceneSingleton<RolloballManager>
    {
        private const string HighscoreName = "Highscore";

        private void Start()
        {
            MessageDistributor.AddObserver(MessageType.PlayerDied, ResetGame);
        }

        private void ResetGame()
        {
            SaveHighscoreIfNeeded();
            InventoryManager.Instance.LoseAllCollectibles();
            foreach (var collectible in FindObjectsOfType<Collectible>(true))
            {
                collectible.gameObject.SetActive(true);
            }
        }

        private void SaveHighscoreIfNeeded()
        {
            var highscoreUntilNow = GetHighscore();
            var currentScore = InventoryManager.Instance.GetCurrentWorth();
            if (currentScore <= highscoreUntilNow) return;
            PlayerPrefs.SetFloat(HighscoreName, currentScore);
            MessageDistributor.Post(MessageType.HighscoreChanged);
        }

        public float GetHighscore()
        {
            return PlayerPrefs.GetFloat(HighscoreName, 0f);
        }
    }
}
