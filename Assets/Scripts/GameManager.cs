using Assets.Scripts.States;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {
        private Animator _animator;
        private Controls _controls;

        public static GameManager Instance;

        private void Awake()
        {
            if(Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            Instance = this;

            _animator = GetComponent<Animator>();
            _controls = new Controls();
            _controls.Enable();
            _controls.Overall.Back.performed += context => GoBack();
        }

        public void GoToMinigameSelection()
        {
            _animator.SetTrigger(MinigameSelectionState.TriggerName);
        }

        public void GoToPlay()
        {
            _animator.SetTrigger(PlayState.TriggerName);
        }

        public void GoToSettings()
        {
            _animator.SetTrigger(SettingsState.TriggerName);
        }

        private void GoBack()
        {
            _animator.SetTrigger("GoBack");
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}
