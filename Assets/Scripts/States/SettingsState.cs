using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.States
{
    public class SettingsState : BaseState
    {
        public const string TriggerName = "GoToSettings";
        public const string MinigameSelectionCanvasName = "SettingsCanvas";

        protected override string SceneName => "MainMenu";

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateExit(animator, stateInfo, layerIndex);
            GetMinigameSelectionCanvas().SetActive(false);
        }

        protected override void OnStateEnterAndSceneReady(Scene scene, LoadSceneMode mode)
        {
            GetMinigameSelectionCanvas().SetActive(true);
            var invertYAxisToggle = GameObject.Find("InvertYAxisToggle");
            GameObject.Find("EventSystem").GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(invertYAxisToggle);

        }

        private GameObject GetMinigameSelectionCanvas()
        {
            return FindObjectsOfType<Canvas>(true).Single(canvas => canvas.gameObject.name == MinigameSelectionCanvasName).gameObject;
        }
    }
}
