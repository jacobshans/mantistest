using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.States
{
    public class MainMenuState : BaseState
    {
        public const string TriggerName = "GoToMainMenu";
        public const string MainMenuCanvasName = "MainMenuCanvas";
        protected override string SceneName => "MainMenu";


        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            GetMainMenuCanvas().SetActive(false);
        }

        protected override void OnStateEnterAndSceneReady(Scene scene, LoadSceneMode mode)
        {
            GetMainMenuCanvas().SetActive(true);
            var selectMinigameButton = GameObject.Find("SelectMinigameButton");
            GameObject.Find("EventSystem").GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(selectMinigameButton);
        }

        private GameObject GetMainMenuCanvas()
        {
            return FindObjectsOfType<Canvas>(true).Single(canvas => canvas.gameObject.name == MainMenuCanvasName).gameObject;
        }
    }
}
