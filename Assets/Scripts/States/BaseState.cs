using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.States
{
    public abstract class BaseState : StateMachineBehaviour
    {
        protected abstract string SceneName { get; }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (SceneManager.GetActiveScene().name == SceneName)
            {
                OnStateEnterAndSceneReady(SceneManager.GetActiveScene());
            }
            else
            {
                SceneManager.sceneLoaded += OnStateEnterAndSceneReady;
                SceneManager.LoadScene(SceneName);
            }
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            SceneManager.sceneLoaded -= OnStateEnterAndSceneReady;
            MessageDistributor.ClearMessageTable();
        }

        protected abstract void OnStateEnterAndSceneReady(Scene scene, LoadSceneMode mode = LoadSceneMode.Single);
    }
}
