using UnityEngine.SceneManagement;

namespace Assets.Scripts.States
{
    public class PlayState : BaseState
    {
        public const string TriggerName = "GoToPlay";

        protected override string SceneName => "Level";

        protected override void OnStateEnterAndSceneReady(Scene scene, LoadSceneMode mode = LoadSceneMode.Single)
        {
        }
    }
}
