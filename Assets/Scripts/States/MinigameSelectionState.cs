using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.States
{
    public class MinigameSelectionState : BaseState
    {
        public const string TriggerName = "GoToMinigameSelection";
        public const string MinigameSelectionCanvasName = "MinigameSelectionCanvas";

        protected override string SceneName => "MainMenu";

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateExit(animator, stateInfo, layerIndex);
            GetMinigameSelectionCanvas().SetActive(false);
        }

        protected override void OnStateEnterAndSceneReady(Scene scene, LoadSceneMode mode)
        {
            GetMinigameSelectionCanvas().SetActive(true);
            var rolloballButton = GameObject.Find("RolloballButton");
            GameObject.Find("EventSystem").GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(rolloballButton);

        }

        private GameObject GetMinigameSelectionCanvas()
        {
            return FindObjectsOfType<Canvas>(true).Single(canvas => canvas.gameObject.name == MinigameSelectionCanvasName).gameObject;
        }
    }
}
