﻿namespace Assets.Scripts.Physics
{
    public class MySphereCollider : MyAbstractCollider
    {
        public float radius;
        public float RadiusWithScale => radius * transform.localScale.x;

        public override CollisionResult CheckCollisionWithSphere(MySphereCollider sphereCollider)
        {
            return CollisionDetector.DetectBetweenSphereAndSphere(this, sphereCollider);
        }

        public override CollisionResult CheckCollisionWithCube(MyCubeCollider cubeCollider)
        {
            return CollisionDetector.DetectBetweenCubeAndSphere(cubeCollider, this);
        }
    }
}