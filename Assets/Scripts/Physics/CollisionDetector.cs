﻿using UnityEngine;

namespace Assets.Scripts.Physics
{
    public class CollisionDetector
    {
        public CollisionResult DetectBetweenSphereAndSphere(MySphereCollider sphereCollider1, MySphereCollider sphereCollider2)
        {
            var distanceSqr = (sphereCollider1.transform.position - sphereCollider2.transform.position).sqrMagnitude;
            var minDistanceWithoutCollisionSqr = Mathf.Pow(sphereCollider1.RadiusWithScale + sphereCollider2.RadiusWithScale, 2);
            if (distanceSqr > minDistanceWithoutCollisionSqr) return CollisionResult.ForNoCollision();
            var normal = (sphereCollider1.transform.position - sphereCollider2.transform.position).normalized;
            var depth = Mathf.Sqrt(minDistanceWithoutCollisionSqr - distanceSqr);
            var position = sphereCollider1.transform.position + (sphereCollider2.transform.position - sphereCollider1.transform.position).normalized * sphereCollider1.RadiusWithScale;
            return CollisionResult.ForCollision(sphereCollider1.MyRigidbody, sphereCollider2.MyRigidbody, normal, depth, position);
        }

        public CollisionResult DetectBetweenCubeAndSphere(MyCubeCollider cubeCollider, MySphereCollider sphereCollider)
        {
            var sphereCenter = sphereCollider.transform.position;
            var closestPosition = ClosestPositionAabbToPoint(sphereCenter, cubeCollider);
            var distanceSqr = (closestPosition - sphereCenter).sqrMagnitude;
            
            if (distanceSqr > sphereCollider.RadiusWithScale * sphereCollider.RadiusWithScale) return CollisionResult.ForNoCollision();
            var normal = (closestPosition - sphereCenter).normalized;
            var depth = sphereCollider.RadiusWithScale - Mathf.Sqrt(distanceSqr);
            return CollisionResult.ForCollision(cubeCollider.MyRigidbody, sphereCollider.MyRigidbody, normal, depth, closestPosition);
        }

        private static Vector3 ClosestPositionAabbToPoint(Vector3 point, MyCubeCollider cubeCollider)
        {
            var localPoint = point - cubeCollider.transform.position;
            var closestPosition = Vector3.zero;

            var v = localPoint.x;
            if (v < cubeCollider.MinX) v = cubeCollider.MinX;
            if (v > cubeCollider.MaxX) v = cubeCollider.MaxX;
            closestPosition.x = v;

            var w = localPoint.y;
            if (w < cubeCollider.MinY) w = cubeCollider.MinY;
            if (w > cubeCollider.MaxY) w = cubeCollider.MaxY;
            closestPosition.y = w;

            var a = localPoint.z;
            if (a < cubeCollider.MinZ) a = cubeCollider.MinZ;
            if (a > cubeCollider.MaxZ) a = cubeCollider.MaxZ;
            closestPosition.z = a;

            return closestPosition + cubeCollider.transform.position;
        }
    }
}