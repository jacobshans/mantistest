﻿using System;
using UnityEngine;

namespace Assets.Scripts.Physics
{
    public abstract class MyAbstractCollider : MonoBehaviour
    {
        public bool isTrigger;
        protected readonly CollisionDetector CollisionDetector;
        public MyRigidbody MyRigidbody { get; private set; }

        protected MyAbstractCollider()
        {
            CollisionDetector = new CollisionDetector();
        }

        private void Awake()
        {
            MyRigidbody = GetComponent<MyRigidbody>();
        }

        public abstract CollisionResult CheckCollisionWithSphere(MySphereCollider sphereCollider);
        public abstract CollisionResult CheckCollisionWithCube(MyCubeCollider cubeCollider);
    }

    public class CollisionResult
    {
        public MyRigidbody Rigidbody1 { get; set; }
        public MyRigidbody Rigidbody2 { get; set; }
        public bool CollisionHappened { get; set; }
        public Vector3 Normal { get; set; }
        public float Depth { get; set; }
        public Vector3 Position { get; set; }

        public bool OnlyTrigger => Rigidbody1.Collider.isTrigger || Rigidbody2.Collider.isTrigger;

        public static CollisionResult ForNoCollision() => new CollisionResult() { CollisionHappened = false };
        public static CollisionResult ForCollision(MyRigidbody rigidbody1, MyRigidbody rigidbody2, Vector3 normal, float depth, Vector3 position) => new CollisionResult()
        {
            Rigidbody1 = rigidbody1,
            Rigidbody2 = rigidbody2,
            CollisionHappened = true,
            Normal = normal,
            Depth = depth,
            Position = position
        };

        public Vector3 CalculateImpulseForFirstBody()
        {
            var relativeVelocity = Rigidbody2.GetVelocity() - Rigidbody1.GetVelocity();
            var relativeVelocityAlongNormal = Vector3.Dot(relativeVelocity, Normal);
            if (relativeVelocityAlongNormal < 0)
                return Vector3.zero;
            var combinedRestitution = Math.Min(Rigidbody1.GetRestitutionCoefficient(), Rigidbody2.GetRestitutionCoefficient());
            var impulseMagnitude = -(1 + combinedRestitution) * relativeVelocityAlongNormal / (Rigidbody1.GetInverseMass() + Rigidbody2.GetInverseMass());
            return impulseMagnitude * Normal;
        }

        public Vector3 CalculateCorrectionForFirstBody()
        {
            var correctionForFirstBody = Math.Max(Depth, 0f) / (Rigidbody1.GetInverseMass() + Rigidbody2.GetInverseMass()) * Normal;
            return correctionForFirstBody;
        }
    }
}