using UnityEngine;

namespace Assets.Scripts.Physics
{
    public class MyRigidbody : MonoBehaviour
    {
        [SerializeField] private float mass;
        [SerializeField] private float angularDrag;
        [SerializeField] private float drag;
        [SerializeField] private float restitutionCoefficient;
        [SerializeField] private bool useGravity;
        [SerializeField] private bool isKinematic;
        [SerializeField] private float maximumVelocity;

        private const float GravitationalAcceleration = 9.81f;

        public MyAbstractCollider Collider { get; private set; }
        private Vector3 _velocity;
        private Vector3 _angularVelocity;
        public bool IsGrounded { get; set; }

        private void Start()
        {
            Collider = GetComponent<MyAbstractCollider>();
        }

        private void OnEnable()
        {
            PhysicsManager.Instance.RegisterRigidbody(this);
        }

        private void OnDisable()
        {
            PhysicsManager.Instance?.DeregisterRigidbody(this);
        }

        public void Tick()
        {
            if (isKinematic) return;
            if (useGravity) MoveGravity();
            MoveRegular();
            Rotate();
            ClampVelocity();
        }

        private void ClampVelocity()
        {
            var twoDeeVelocity = new Vector2(_velocity.x, _velocity.z);
            var clampedTwoDeeVelocity = twoDeeVelocity.normalized * Mathf.Clamp(twoDeeVelocity.magnitude, 0, maximumVelocity);
            _velocity = new Vector3(clampedTwoDeeVelocity.x, _velocity.y, clampedTwoDeeVelocity.y);
        }

        private void Rotate()
        {
            var position = transform.position;
            transform.RotateAround(position, Vector3.right, _angularVelocity.x);
            transform.RotateAround(position, Vector3.up, _angularVelocity.y);
            transform.RotateAround(position, Vector3.forward, _angularVelocity.z);
            _angularVelocity *= 1 - angularDrag;
        }

        private void MoveRegular()
        {
            var traveledSincePreviousTick = _velocity * Time.deltaTime;
            transform.position += new Vector3(traveledSincePreviousTick.x, traveledSincePreviousTick.y, traveledSincePreviousTick.z);
            _velocity *= 1 - drag;
        }

        private void MoveGravity()
        {
            _velocity += GravitationalAcceleration * Time.deltaTime * Vector3.down;
        }

        public CollisionResult IsCollidingWith(MyRigidbody myRigidbody)
        {
            if (myRigidbody.Collider is MySphereCollider sphereCollider) return Collider.CheckCollisionWithSphere(sphereCollider);
            if (myRigidbody.Collider is MyCubeCollider cubeCollider) return Collider.CheckCollisionWithCube(cubeCollider);
            return CollisionResult.ForNoCollision();
        }

        public void AddForce(Vector3 force)
        {
            if (isKinematic) return;
            _velocity += force * Time.deltaTime / mass;
        }

        public void AddTorque(Vector3 torque)
        {
            _angularVelocity.x += torque.x / mass;
            _angularVelocity.y += torque.y / mass;
            _angularVelocity.z += torque.z / mass;
        }

        public void AddImpulse(Vector3 impulse)
        {
            if (isKinematic) return;
            _velocity += impulse / mass;
        }

        public Vector3 GetVelocity()
        {
            return _velocity;
        }

        public bool IsKinematic()
        {
            return isKinematic;
        }

        public void ResetYVelocity()
        {
            _velocity.y = 0f;
        }

        public void Reset()
        {
            _velocity = Vector3.zero;
            _angularVelocity = Vector3.zero;
        }

        public float GetRestitutionCoefficient()
        {
            return restitutionCoefficient;
        }

        public float GetInverseMass()
        {
            if (isKinematic) return 0;
            return 1 / mass;
        }

        public void CorrectPositionAfterCollision(Vector3 correction)
        {
            if (isKinematic) return;
            transform.position += correction / mass;
        }

        public void AddFrictionAtPoint(Vector3 position)
        {
            var frictionForce = Vector3.Cross(_angularVelocity, transform.position - position);
            AddForce(frictionForce * 2f);
        }
    }
}
