﻿using System;
using UnityEngine;

namespace Assets.Scripts.Physics
{
    public class MyCubeCollider : MyAbstractCollider
    {
        [SerializeField] private Vector3 dimensions;
        public Vector3 DimensionsWithScale => new Vector3(dimensions.x * transform.localScale.x, dimensions.y * transform.localScale.y, dimensions.z * transform.localScale.z);
        public float MinX => -MaxX;
        public float MaxX => DimensionsWithScale.x / 2f;
        public float MinY => -MaxY;
        public float MaxY => DimensionsWithScale.y / 2f;
        public float MinZ => -MaxZ;
        public float MaxZ => DimensionsWithScale.z / 2f;

        private void Start()
        {
            if (transform.rotation.Equals(Quaternion.identity)) return;
            throw new Exception("Custom physics engine does not currently support cube colliders with non-zero rotation");
        }

        public override CollisionResult CheckCollisionWithSphere(MySphereCollider sphereCollider)
        {
            return CollisionDetector.DetectBetweenCubeAndSphere(this, sphereCollider);
        }

        public override CollisionResult CheckCollisionWithCube(MyCubeCollider sphereCollider)
        {
            return CollisionResult.ForNoCollision();
        }
    }
}