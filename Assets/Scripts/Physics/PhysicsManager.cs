using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Inventory;
using UnityEngine;

namespace Assets.Scripts.Physics
{
    public class PhysicsManager : SceneSingleton<PhysicsManager>
    {
        private readonly List<MyRigidbody> _allRegisteredRigidbodies = new List<MyRigidbody>();
        private const string CollisionMethodName = nameof(InventoryManager.OnTriggerTriggered);

        private void FixedUpdate()
        {
            var collisionResults = new List<CollisionResult>();
            foreach (var myRigidbody in _allRegisteredRigidbodies)
            {
                myRigidbody.IsGrounded = false;
                foreach (var otherRigidbody in _allRegisteredRigidbodies)
                {
                    if (CollisionAlreadyChecked(myRigidbody, otherRigidbody, collisionResults)) continue;
                    if (myRigidbody == otherRigidbody) continue;
                    var collisionResult = myRigidbody.IsCollidingWith(otherRigidbody);
                    if (!collisionResult.CollisionHappened) continue;
                    collisionResults.Add(collisionResult);
                }
            }
            foreach (var collisionResult in collisionResults)
            {
                if (collisionResult.OnlyTrigger)
                {
                    NotifyGameObjectsOfTrigger(collisionResult);
                    continue;
                }
                ResolveCollision(collisionResult);
                SetIsGroundedOnRigidbodies(collisionResult);
            }
            foreach (var myRigidbody in _allRegisteredRigidbodies)
            {
                myRigidbody.Tick();
            }
        }

        private void NotifyGameObjectsOfTrigger(CollisionResult collisionResult)
        {
            collisionResult.Rigidbody1.gameObject.SendMessage(CollisionMethodName, collisionResult.Rigidbody2, SendMessageOptions.DontRequireReceiver);
            collisionResult.Rigidbody2.gameObject.SendMessage(CollisionMethodName, collisionResult.Rigidbody1, SendMessageOptions.DontRequireReceiver);
        }

        private static void SetIsGroundedOnRigidbodies(CollisionResult collisionResult)
        {
            if (Mathf.Abs(collisionResult.Normal.y) < .5f) return;
            if (collisionResult.Rigidbody1.IsKinematic()) collisionResult.Rigidbody2.IsGrounded = true;
            if (collisionResult.Rigidbody2.IsKinematic()) collisionResult.Rigidbody1.IsGrounded = true;
        }

        private static void ResolveCollision(CollisionResult collisionResult)
        {
            var correctionForFirstBody = collisionResult.CalculateCorrectionForFirstBody();
            collisionResult.Rigidbody1.CorrectPositionAfterCollision(correctionForFirstBody);
            collisionResult.Rigidbody2.CorrectPositionAfterCollision(-correctionForFirstBody);

            var impulseForFirstBody = collisionResult.CalculateImpulseForFirstBody();
            collisionResult.Rigidbody1.AddImpulse(-impulseForFirstBody);
            collisionResult.Rigidbody2.AddImpulse(impulseForFirstBody);
            collisionResult.Rigidbody1.AddFrictionAtPoint(collisionResult.Position);
            collisionResult.Rigidbody2.AddFrictionAtPoint(collisionResult.Position);
        }

        private bool CollisionAlreadyChecked(MyRigidbody rigidbody1, MyRigidbody rigidbody2, List<CollisionResult> alreadyChecked)
        {
            return alreadyChecked.Any(result =>
                (rigidbody1 == result.Rigidbody1 || rigidbody1 == result.Rigidbody2) &&
                (rigidbody2 == result.Rigidbody1 || rigidbody2 == result.Rigidbody2));
        }

        public void RegisterRigidbody(MyRigidbody myRigidbody)
        {
            _allRegisteredRigidbodies.Add(myRigidbody);
        }
        public void DeregisterRigidbody(MyRigidbody myRigidbody)
        {
            _allRegisteredRigidbodies.Remove(myRigidbody);
        }
    }
}
