using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInputDelegator : MonoBehaviour
    {
        private Controls _controls;
        private Vector2 _currentMoveInput;
        private Vector2 _currentLookRotationInput;
        private PlayerController _playerController;
        private FollowTargetRotator _followTargetRotator;

        private void Awake()
        {
            _controls = new Controls();
            _controls.Gameplay.HorizontalMovement.performed += context => _currentMoveInput = context.ReadValue<Vector2>();
            _controls.Gameplay.LookRotation.performed += context => _currentLookRotationInput = context.ReadValue<Vector2>();
            _controls.Gameplay.Jump.performed += _ => Jump();

            _playerController = GetComponent<PlayerController>();
            _followTargetRotator = GetComponentInChildren<FollowTargetRotator>();
        }

        private void Jump()
        {
            _playerController.Jump();
        }

        private void FixedUpdate()
        {
            DelegateMovementPerformed(_currentMoveInput);
        }

        private void LateUpdate()
        {
            _followTargetRotator.SetRotation(_currentLookRotationInput);
        }

        private void OnEnable()
        {
            _controls.Enable();
        }

        private void OnDisable()
        {
            _controls.Disable();
        }

        private void DelegateMovementPerformed(Vector2 movement)
        {
            _playerController.Move(movement);
        }
    }
}
