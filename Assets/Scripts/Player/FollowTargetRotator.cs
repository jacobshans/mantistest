using UnityEngine;

namespace Assets.Scripts.Player
{
    public class FollowTargetRotator : MonoBehaviour
    {
        [SerializeField] private float cameraSensitivity;
        private float _yRotation;
        private float _xRotation;
        private bool _invertYAxis;

        private void Awake()
        {
            _invertYAxis = PlayerPrefs.GetInt("InvertYAxis") == 1;
        }
        
        public void SetRotation(Vector2 rotationInput)
        {
            _yRotation += rotationInput.x * cameraSensitivity;
            _xRotation -= rotationInput.y * cameraSensitivity;

            transform.rotation = Quaternion.Euler(_xRotation * (_invertYAxis ? -1f : 1f), _yRotation, 0f);
        }
    }
}
