using Assets.Scripts.Physics;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float rotationForce;
        [SerializeField] private float jumpForce;
        private MyRigidbody _myRigidbody;
        private Transform _mainCameraTransform;
        private Vector3 _startPosition;

        private void Awake()
        {
            _myRigidbody = GetComponent<MyRigidbody>();
            _mainCameraTransform = Camera.main.gameObject.transform;
            _startPosition = transform.position;
        }

        public void Move(Vector2 movement)
        {
            var torque = _mainCameraTransform.right * movement.y - _mainCameraTransform.forward * movement.x;
            _myRigidbody.AddTorque(torque * rotationForce);
        }

        public void Jump()
        {
            if (!_myRigidbody.IsGrounded) return;
            _myRigidbody.ResetYVelocity();
            _myRigidbody.AddImpulse(Vector3.up * jumpForce);
        }

        private void FixedUpdate()
        {
            if (transform.position.y > -5f) return;
            MessageDistributor.Post(MessageType.PlayerDied);
            _myRigidbody.Reset();
            transform.position = _startPosition;
        }
    }
}
