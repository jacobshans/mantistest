﻿using UnityEngine;

namespace Assets.Scripts
{
    public abstract class SceneSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                }

                return _instance;
            }
        }

        private static T _instance;

        private void OnDestroy()
        {
            _instance = null;
        }
    }
}