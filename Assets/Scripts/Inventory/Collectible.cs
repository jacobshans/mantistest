using UnityEngine;

namespace Assets.Scripts.Inventory
{
    public class Collectible : MonoBehaviour
    {
        [SerializeField] private CollectibleType type;

        private MeshRenderer _meshRenderer;
        public CollectibleInformation CollectibleInformation { get; private set; }

        private void Start()
        {
            _meshRenderer = GetComponentInChildren<MeshRenderer>();
            var information = InventoryManager.Instance.GetCollectibleInformation(type);
            Initialize(information);
        }

        private void Initialize(CollectibleInformation collectibleInformation)
        {
            CollectibleInformation = collectibleInformation;
            gameObject.name = collectibleInformation.type.ToString();
            ChangeColorTo(collectibleInformation.color);
        }

        private void ChangeColorTo(Color color)
        {
            var materialPropertyBlock = new MaterialPropertyBlock();
            _meshRenderer.GetPropertyBlock(materialPropertyBlock);
            materialPropertyBlock.SetColor("_BaseColor", color);
            _meshRenderer.SetPropertyBlock(materialPropertyBlock);
        }
    }
}