using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Physics;
using UnityEngine;

namespace Assets.Scripts.Inventory
{
    public class InventoryManager : SceneSingleton<InventoryManager>
    {
        private Dictionary<CollectibleInformation, int> _amountOfCollectiblesPerType = new Dictionary<CollectibleInformation, int>();
        private List<CollectibleInformation> _cachedCollectibleInformations = new List<CollectibleInformation>();

        private void Awake()
        {
            FillCollectiblesCache();
            _amountOfCollectiblesPerType = GetPrepopulatedAmounts();
        }

        public void OnTriggerTriggered(MyRigidbody otherRigidbody)
        {
            var collectible = otherRigidbody.GetComponent<Collectible>();
            AddCollectibleToInventory(collectible);
            MessageDistributor.Post(MessageType.InventoryChanged);
            collectible.gameObject.SetActive(false);
        }

        private void AddCollectibleToInventory(Collectible collectible)
        {
            var information = collectible.CollectibleInformation;
            if (_amountOfCollectiblesPerType.ContainsKey(information))
                _amountOfCollectiblesPerType[information]++;
            else
                _amountOfCollectiblesPerType[information] = 1;
        }

        public Dictionary<CollectibleInformation, int> GetStatus()
        {
            return _amountOfCollectiblesPerType;
        }

        public void LoseAllCollectibles()
        {
            _amountOfCollectiblesPerType = GetPrepopulatedAmounts();
            MessageDistributor.Post(MessageType.InventoryChanged);
        }

        private Dictionary<CollectibleInformation, int> GetPrepopulatedAmounts()
        {
            var newAmounts = new Dictionary<CollectibleInformation, int>();
            foreach (var information in _cachedCollectibleInformations)
            {
                newAmounts[information] = 0;
            }
            return newAmounts;
        }

        private void FillCollectiblesCache()
        {
            _cachedCollectibleInformations = Resources.LoadAll<CollectibleInformation>("CollectibleInformations").ToList();
        }

        public CollectibleInformation GetCollectibleInformation(CollectibleType type)
        {
            return _cachedCollectibleInformations.Single(info => info.type == type);
        }

        public float GetCurrentWorth()
        {
            var totalWorth = 0f;
            foreach (var pair in _amountOfCollectiblesPerType)
            {
                totalWorth += pair.Key.worth * pair.Value;
            }

            return totalWorth;
        }
    }

    public enum CollectibleType
    {
        Wood,
        Gold,
        Brass,
        Diamond
    }
}
