using UnityEngine;

namespace Assets.Scripts.Inventory
{
    [CreateAssetMenu(fileName = "CollectibleInformation", menuName = "ScriptableObjects/CollectibleInformationScriptableObject", order = 1)]
    public class CollectibleInformation : ScriptableObject
    {
        public CollectibleType type;
        public Color color;
        public float worth;
    }
}
