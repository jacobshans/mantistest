using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Inventory
{
    public class InventoryVisualiser : MonoBehaviour
    {
        [SerializeField] private Transform groupRoot;
        [SerializeField] private TMP_Text totalWorthText;

        private readonly Dictionary<CollectibleInformation, TMP_Text> _textPerCollectibleInformation = new Dictionary<CollectibleInformation, TMP_Text>();
        
        private void Start()
        {
            MessageDistributor.AddObserver(MessageType.InventoryChanged, UpdateCollectibles);
        }

        private void UpdateCollectibles()
        {
            var inventoryStatus = InventoryManager.Instance.GetStatus();
            
            foreach (var (information, amount) in inventoryStatus)
            {
                if (amount == 0)
                    RemoveVisualisationForCollectible(information);
                else if (amount > 1)
                    UpdateExistingCollectibleUi(information, amount);
                else if(!_textPerCollectibleInformation.ContainsKey(information))
                    ShowNewCollectible(information);
            }

            totalWorthText.text = $"${InventoryManager.Instance.GetCurrentWorth()}";
        }

        private void RemoveVisualisationForCollectible(CollectibleInformation information)
        {
            if (!_textPerCollectibleInformation.ContainsKey(information)) return;

            var collectibleUI = _textPerCollectibleInformation[information].transform.parent.parent.gameObject;
            Destroy(collectibleUI);
            _textPerCollectibleInformation.Remove(information);
        }

        private void UpdateExistingCollectibleUi(CollectibleInformation information, int amount)
        {
            _textPerCollectibleInformation[information].text = "x" + amount;
        }

        private void ShowNewCollectible(CollectibleInformation information)
        {
            var collectibleUI = Instantiate(Resources.Load<GameObject>("Prefabs/CollectibleUI"), groupRoot, true);
            collectibleUI.GetComponentInChildren<Image>().color = information.color;
            _textPerCollectibleInformation[information] = collectibleUI.GetComponentInChildren<TMP_Text>();
        }

        private void OnDestroy()
        {
            MessageDistributor.RemoveObserver(MessageType.InventoryChanged, UpdateCollectibles);
        }
    }
}
