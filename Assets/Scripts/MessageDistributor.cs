using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public static class MessageDistributor
    {
        private static readonly Dictionary<MessageType, List<Action>> MessageTable = new();
    
        public static void AddObserver(MessageType messageType, Action handler)
        {
            List<Action> list = null;
            if (!MessageTable.TryGetValue(messageType, out list))
            {
                list = new List<Action>();
                MessageTable.Add(messageType, list);
            }

            if (!list.Contains(handler))
                MessageTable[messageType].Add(handler);
        }

        public static void RemoveObserver(MessageType messageType, Action handler)
        {
            List<Action> list = null;
            if (!MessageTable.TryGetValue(messageType, out list)) return;

            if (list.Contains(handler))
                list.Remove(handler);
        }

        public static void Post(MessageType messageType)
        {
            List<Action> list = null;
            if (!MessageTable.TryGetValue(messageType, out list)) return;

            for (var i = list.Count - 1; i >= 0; i--)
                list[i]();
        }

        public static void ClearMessageTable()
        {
            MessageTable.Clear();
        }
    }

    public enum MessageType
    {
        InventoryChanged,
        PlayerDied,
        HighscoreChanged
    }
}