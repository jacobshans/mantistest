using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class ButtonClickDistributor : MonoBehaviour
    {
        private Toggle _invertYAxisToggle;

        private void Awake()
        {
            _invertYAxisToggle = FindObjectOfType<Toggle>(true);
            var invertYAxis = PlayerPrefs.GetInt("InvertYAxis") == 1;
            _invertYAxisToggle.SetIsOnWithoutNotify(invertYAxis);
        }

        public void GoToMinigameSelection()
        {
            GameManager.Instance.GoToMinigameSelection();
        }

        public void GoToPlay()
        {
            GameManager.Instance.GoToPlay();
        }

        public void GoToSettings()
        {
            GameManager.Instance.GoToSettings();
        }

        public void Quit()
        {
            GameManager.Instance.Quit();
        }

        public void InvertYAxisValueChanged()
        {
            PlayerPrefs.SetInt("InvertYAxis", _invertYAxisToggle.isOn ? 1 : 0);
        }
    }
}
